# Usage

idaq -A -OIDAPython:./dumpprocs.py <binary>

# Output

<md5>-proc.csv: A CSV containing a listing of the procedures found in
                binary with <md5>
<md5>-proc/:    Every file in this folder is a disassembled procedure from
                binary md5, named using the start virtual address (VA)
# Utility Scripts
These are some small scripts given mostly as examples on how to perform some 
transformations of the output of dumpprocs. 

These scripts are provided without warrenty or guarantee of compatability.

* **filter-unnamed.sh**: Removed procedures named sub_*  or null* from csv.
* **list-procs.sh**: Created a direct listing of procedure names from csv
* **remove-params.sh**: Removed paramenters from procedure names

