"""
Dump a "map file" of the procedures as found by IDA.
Creates a .csv file and a directory of disassembled procedures.

New blocks are created at call instructions for the purpose of # Blocks

@author Craig Miles, Charles LeDoux
@maintainer Charles LeDoux
"""
import idawrapper
import re
import os.path
import csv

PROC_HEADER = ("MD5", "IDA Label", "RVA", "VA", "# Edges", "# Blocks", "# Instructions" )

CSV_FILE = "%s-procs.csv"
CSV_FILE_NOLIBS = "%s-procs-nolibs.csv"
PROC_DIR = "./%s-procs"
PROC_DIR_NOLIBS = "./%s-procs-nolibs"

def signed(n):
    """Converts an unsigned int into a signed int.
       This is necessary because IDAPython's GetOperandValue
       incorrecty returns negative offsets as postive unsigned ints"""
    val = struct.unpack("i", struct.pack("I", n))[0]
    return val

def parseInstr(ea):
    """Parses the instruction at ea into Prolog syntax and returns it."""

    # From optype_t definition in idasdk\includes\ua.hpp
    VOID_OPND   = 0  # No Operand
    REG_OPND    = 1  # General Register (includes XMM & MM regs)
    MEM_OPND    = 2  # Direct Memory Reference  (DATA)
    PHRASE_OPND = 3  # Memory Ref [Base Reg + Index Reg]
    DISPL_OPND  = 4  # Memory Ref [Base Reg + Index Reg + Displacement]
    IMM_OPND    = 5  # Immediate Value
    FAR_OPND    = 6  # Immediate Far Address  (CODE)
    NEAR_OPND   = 7  # Immediate Near Address (CODE)
    FP_OPND     = 11 # Floating Point (ST) Register

    prologInstr = ida.getMnemonic(ea)
    isFirstOp = True
    sawOps = False
    i = 0
    op_type = ida.getOperandType(ea, i)

    # This is to catch IDA weirdness where FP instructions like fldz, fstp, & fucompp have empty
    # op_type 11 operands.
    while (op_type == FP_OPND) and (ida.getOperand(ea,i) == ""):
        i = i + 1
        op_type = ida.getOperandType(ea, i)

    while(op_type != VOID_OPND):
        sawOps = True
        if isFirstOp:
            prologInstr = prologInstr + "("
        if not isFirstOp:
            prologInstr = prologInstr + ","
        #endif
        # Op is register
        if op_type == REG_OPND:
            prologInstr = prologInstr + ida.lookupRegFromRegNum(ida.getOperandValue(ea, i))
        #endif
        # Op is direct memory reference
        elif op_type == MEM_OPND:
            opnd = ida.getOperand(ea, i)
            if re.match(".*fs:.*", opnd):
                seg = "fs"
            elif re.match(".*qs:.*", opnd):
                seg = "qs"
            else:
                seg = "ds"
            val = ida.getOperandValue(ea, i)
            prologInstr = prologInstr + "none(0x" + ("%x" % val) + "," + seg + ")"
        #endif
        # Memory Ref [Reg]
        elif op_type == PHRASE_OPND:
            opnd = ida.getOperand(ea, i)
            if re.match("dword ptr.*", opnd):
                ptrtype = "dptr"
            elif re.match("word ptr.*", opnd):
                ptrtype = "wptr"
            elif re.match("byte ptr.*", opnd):
                ptrtype = "bptr"
            else:
                ptrtype = "none"
            reg = ida.lookupRegFromRegNum(ida.getOperandValue(ea, i))
            prologInstr = prologInstr + ptrtype + "(" + reg + ")"

        # Memory Ref [Base Reg + Displacement]
        elif op_type == DISPL_OPND:
            ida.setOperandDisplayModeToSegment(ea, i)
            opnd = ida.getOperand(ea, i)
            if re.match("dword ptr.*", opnd):
                ptrtype = "dptr"
            elif re.match("word ptr.*", opnd):
                ptrtype = "wptr"
            elif re.match("byte ptr.*", opnd):
                ptrtype = "bptr"
            else:
                ptrtype = "none"
            match = re.match(".*\[([a-z\+]+)([\+-]).*", opnd)
            if match:
                baseReg = match.group(1)
                operation = match.group(2)
                displacement = ida.getOperandValue(ea,i)
                if operation == "-":
                    displacement = -signed(displacement)
                prologInstr = prologInstr + ptrtype + "(" + baseReg + operation + "0x" + ("%x" % displacement) + ")"
            else:
                return None

        # Immediate
        elif op_type == IMM_OPND or op_type == FAR_OPND or op_type == NEAR_OPND:
            prologInstr = prologInstr + "0x" + ("%x" % ida.getOperandValue(ea,i))

        # FP Reg (st0 - st7)
        elif op_type == FP_OPND:
            opnd = ida.getOperand(ea, i)
            if opnd == "st":
                prologInstr = prologInstr + "st0"
            else:
                match = re.match("st\(([1-7])\)", opnd)
                if match:
                    stregnum = match.group(1)
                    prologInstr = prologInstr + "st" + stregnum
                else:
                    return None

        # Unknown operand op_type
        else:
            return None

        isFirstOp = False
        i = i + 1
        op_type = ida.getOperandType(ea, i)

    if sawOps:
        prologInstr = prologInstr + ")"
    return prologInstr

ida = idawrapper.IDAWrapper()

md5 = ida.getMD5OfInputFile().lower()

if not os.path.exists(PROC_DIR%(md5,)):
    os.mkdir(PROC_DIR%(md5,))
# if not os.path.exists(PROC_DIR_NOLIBS%(md5,)):
#     os.mkdir(PROC_DIR_NOLIBS%(md5,))

csv_file = open(CSV_FILE%(md5,), 'w')
csv_writer = csv.writer(csv_file)
csv_writer.writerow(PROC_HEADER)
# csv_nolibs = open(CSV_FILE_NOLIBS%(md5,), 'w')

for fs_ea in ida.Functions():
    disasm = ""
    edgeCount = 0
    blockCount = 0
    headCount = 0
    func_t = ida.getFuncT(fs_ea)
    ida.removeAllRegisterRenamingsInFunc(func_t)
    CFG = ida.getFuncCFG(func_t)

    for basicBlock in CFG:
        blockCount += 1
        for succ in basicBlock.succs():
            edgeCount += 1
        disasm = disasm + "\nloc_%x:"%(basicBlock.startEA,)
        for ea in ida.Heads(basicBlock.startEA, basicBlock.endEA):
            if ida.isCode(ea):
                headCount = headCount + 1
                if ida.instructionIsACall(ea):
                    blockCount += 1
                    if ida.isAnAPI(ida.getCallTarget(ea)):
                        disasm = disasm + "\n\tcall(" + ida.getAPIName(ida.getCallTarget(ea)) + ")"
                    else:
                        disasm = disasm + "\n\t" + parseInstr(ea)
#                         disasm = disasm + "\n\tcall(" + ida.getFunctionName(ida.getCallTarget(ea)) + ")"
                elif ida.instructionIsAJMPToAPI(ea):
                    disasm = disasm + "\n\tjmp(" + ida.getAPIName(ida.getJmpToAPITarget(ea)) + ")"
                else:
                    disasm = disasm + "\n\t" + parseInstr(ea)

    name = ida.getFunctionName(fs_ea)
    rva = hex(ida.VA2RVA(fs_ea))
    header = (md5,name,rva,str(hex(fs_ea)),str(edgeCount),str(blockCount),str(headCount))
    # if not ida.isAnAPI(fs_ea) and not ida.isLibraryFunction(fs_ea):
    csv_writer.writerow(header)
    proc_file = open(os.path.join(PROC_DIR%(md5,), str(rva)), 'w')
    proc_file.write("# " + ",".join(PROC_HEADER))
    proc_file.write("# " + ",".join(header))
    proc_file.write(disasm)
    proc_file.close()

    # # Always write out to the libs versions
    # csv_nolibs.write(header)
    # csv_nolibs.write('\n')
    # proc_file = open(os.path.join(PROC_DIR_NOLIBS%(md5,), str(rva)), 'w')
    # proc_file.write(header)
    # proc_file.write(disasm)
    # proc_file.close()

csv_file.close()
print("Done dumping procedure")
ida.killIDA()
