#!/bin/bash

# Remove parameters from function names

infile=$1

sed -i -e 's/(.*)//g' $infile
