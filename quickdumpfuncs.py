""" Quickly dump the functions from the binary in a format that can be grepped for on the csvs
"""
import idaapi
import idautils
import idc
import csv

HEADER = ['MD5', 'StartRVA']

imagebase = idautils.peutils_t().imagebase
outf = open('%s.dump'%(idc.GetInputMD5(),), 'a')
out_csv = csv.writer(outf)
out_csv.writerow(HEADER)
for fs_ea in idautils.Functions():
    rva = fs_ea - imagebase
    out_csv.writerow((idc.GetInputMD5().lower(), hex(rva)))

outf.close()
print("Done")
